﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace HotelTesztFeladat
{
	public class FiveDivisableValidation : ValidationRule
	{
		public override ValidationResult Validate(object value, CultureInfo cultureInfo)
		{
			int val = 0;
			if(int.TryParse(value.ToString(), out val))
			{
				if (val % 5 != 0)
				{
					return new ValidationResult(false, "Csak 5-tel osztható érték lehet!");
				}
				else
					return ValidationResult.ValidResult;
			}
			return new ValidationResult(false, "Csak egész szám lehet az érték!");
		}
	}
}

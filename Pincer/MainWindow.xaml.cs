﻿using HotelTesztFeladat.QueryResponses;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Windows;
using System.Windows.Controls;

namespace HotelTesztFeladat
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		/// <summary>
		/// A jelenlegi Id, amivel a műveleteket végezzük
		/// </summary>
		private int currentBraceletID = -1;
		public int CurrentBraceletID
		{
			get { return currentBraceletID; }
			set
			{
				currentBraceletID = value;
				currentBraceletIDLabel.GetBindingExpression(Label.ContentProperty).UpdateTarget();
			}
		}
		/// <summary>
		/// A jelenlegi összeg, amit levonunk az egyenlegből
		/// </summary>
		public int ChargeAmount { get; set; }
		/// <summary>
		/// A jelenlegi összeg, amit feltöltünk az egyenleghez
		/// </summary>
		public int DepositAmount { get; set; }

		public MainWindow()
		{
			InitializeComponent();
			chargeAmountTextBox.Focus();
		}

		/// <summary>
		/// Kezeli a kártya ID beviteli eseményt
		/// </summary>
		private void ReadBracelet_Click(object sender, RoutedEventArgs e)
		{
			int checkingBraceletID = int.Parse(braceletIDTextBox.Text); ;
			var request = new RestRequest("api/check_if_active", Method.POST);
			request.AddParameter("bracelet_id", checkingBraceletID);
			App.Client.ExecuteAsync<BraceletActiveResult>(request, response =>
			{
				UpdateLatestTransaction();
				Console.WriteLine(response.Content);
				Dispatcher.Invoke((Action)(() =>
				{
					if (!string.IsNullOrEmpty(response.Data.Error))
					{
						MessageBox.Show(string.Format("Hiba: {0}", response.Data.Error));
						CurrentBraceletID = -1;
						return;
					}
					if (response.Data.Result)
						CurrentBraceletID = checkingBraceletID;
				}));
			});
		}

		/// <summary>
		/// Kezeli a fizetés eseményt
		/// </summary>
		private void Charge_Click(object sender, RoutedEventArgs e)
		{
			if (CurrentBraceletID == -1 || Validation.GetHasError(chargeAmountTextBox))
				return;
			var request = new RestRequest("api/charge", Method.POST);
			request.AddParameter("full_price", ChargeAmount);
			request.AddParameter("bracelet_id", CurrentBraceletID);
			App.Client.ExecuteAsync<ChargeResponse>(request, response =>
			{
				UpdateLatestTransaction();
				Console.WriteLine(response.Content);
				if (!string.IsNullOrEmpty(response.Data.Error))
				{
					MessageBox.Show(string.Format("Hiba: {0}", response.Data.Error));
					return;
				}
				Console.WriteLine(response.Content);
				MessageBox.Show(string.Format("Sikeres fizetés!\nÚj egyenleg: {0}", response.Data.Balance));
			});
		}

		/// <summary>
		/// Kezeli a sztornó eseményt
		/// </summary>
		private void Storno_Click(object sender, RoutedEventArgs e)
		{
			var request = new RestRequest("api/storno", Method.POST);
			request.AddParameter("bracelet_id", CurrentBraceletID);
			App.Client.ExecuteAsync<StornoResponse>(request, response =>
			{
				UpdateLatestTransaction();
				Console.WriteLine(response.Content);
				if (!string.IsNullOrEmpty(response.Data.Error))
				{
					MessageBox.Show(string.Format("Hiba: {0}", response.Data.Error));
					return;
				}
				MessageBox.Show("A sztornó sikeres.");
			});
		}

		/// <summary>
		/// Kezeli a feltöltés eseményt
		/// </summary>
		private void Deposit_Click(object sender, RoutedEventArgs e)
		{
			if (CurrentBraceletID == -1 || Validation.GetHasError(depositAmountTextBox))
				return;

			var request = new RestRequest("api/deposit", Method.POST);
			request.AddParameter("amount", DepositAmount);
			request.AddParameter("bracelet_id", CurrentBraceletID);
			App.Client.ExecuteAsync<DepositResponse>(request, response =>
			{
				Console.WriteLine(response.Content);
				if (!string.IsNullOrEmpty(response.Data.Error))
				{
					MessageBox.Show(string.Format("Hiba: {0}", response.Data.Error));
					return;
				}
				Console.WriteLine(response.Content);
				MessageBox.Show(string.Format("Sikeres feltöltés!\nÚj egyenleg: {0}", response.Data.Balance));
			});
		}

		/// <summary>
		/// Frissíti az IDhez tartozó legutolsó tranzakciót mezőt.
		/// </summary>
		private void UpdateLatestTransaction()
		{
			if (CurrentBraceletID == -1)
				return;

			var request = new RestRequest("api/latest_transaction", Method.POST);
			request.AddParameter("bracelet_id", CurrentBraceletID);
			App.Client.ExecuteAsync<TransactionResponse>(request, response =>
			{
				Dispatcher.Invoke((Action)(() =>
				{
					//Adatmező törlése
					lastTransaction.Items.Clear();

					if (!string.IsNullOrEmpty(response.Data.Error))
						return;

					//Adatmező kitöltése
					lastTransaction.Items.Add(response.Data);
				}));
			});
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelTesztFeladat.QueryResponses
{
	public class ChargeResponse : MayFailResponse
	{
		public int Balance { get; set; }
	}
}

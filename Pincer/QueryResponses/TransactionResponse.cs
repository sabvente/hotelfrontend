﻿using RestSharp.Deserializers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelTesztFeladat.QueryResponses
{
	public class TransactionResponse : MayFailResponse
	{
		[DeserializeAs(Name = "timestamp")]
		public DateTime Date { get; set; }
		public int Amount { get; set; }
	}
}

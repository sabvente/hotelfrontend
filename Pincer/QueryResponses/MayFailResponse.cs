﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelTesztFeladat.QueryResponses
{
	public class MayFailResponse
	{
		public string Error { get; set; }
	}
}

﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace HotelTesztFeladat
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
		private static RestClient client = new RestClient(ConfigurationManager.AppSettings["ServerAddress"]);

		public static RestClient Client
		{
			get { return client; }
			set { client = value; }
		}
	}
}
